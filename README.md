# ToasterLibKT

## Overview

Toaster Library is the backend library made by FRC Team 3641 based on the popular library by FRC Team 5190. This library was written in the Kotlin JVM Language. Some features of this library include:

 * Wrapped WPILib Commands and Subsystems with Kotlin Coroutines asynchronous optimzation.

 * High level mathematics for path generation, tracking, custom typesafe units of measure, etc.
    * Two-dimensional parametric and functional splines.
    * Arc length of parametric quintic hermite splines evaluated using recursive arc subdivision (from Team 254).
    * Trajectory generation that respects constraints (i.e. centripetal acceleration, motor voltage).
    * Custom trajectory followers
        * Ramsete
        * Adaptive Pure Pursuit
        * Feedforward
    * Typesafe units of measure
        * Quick and easy conversions between all length, velocity, acceleration, electrical units.
        * Support for Talon SRX native unit length and rotation models.

 * AHRS sensor wrapper for Pigeon IMU and NavX.

 * Tank Drive Subsystem abstraction with built-in odometry and command to follow trajectories.

 * Talon SRX & Spark Max wrapper that utilizes Kotlin properties to set configurations.

 * Custom robot base with fully implemented state machine and coroutine support.

 * Other WPILib wrappers for NetworkTables, etc.
 
 * Config file-based motor controller creation and configuration 
 
 * Real-time Logging with Kotlin delegates
 

## Contributing

This library is open source and we would love to have you contribute code to this repository.

Please build the project locally using `./gradlew build` to make sure everything works before submitting a pull request. 

