package org.toaster.lib.motors.rev

import com.revrobotics.CANEncoder
import org.toaster.lib.mathematics.units.SIUnit
import org.toaster.lib.mathematics.units.nativeunits.NativeUnitModel
import org.toaster.lib.motors.AbstractFalconEncoder

class FalconMAXEncoder<T : SIUnit<T>>(
    val canEncoder: CANEncoder,
    model: NativeUnitModel<T>
) : AbstractFalconEncoder<T>(model) {
    override val rawVelocity: Double get() = canEncoder.velocity / 60.0
    override val rawPosition: Double get() = canEncoder.position

    override fun resetPosition(newPosition: Double) {
        canEncoder.position = newPosition
    }

}