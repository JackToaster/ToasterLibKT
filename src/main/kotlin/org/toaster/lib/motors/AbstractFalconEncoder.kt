package org.toaster.lib.motors

import org.toaster.lib.mathematics.units.SIUnit
import org.toaster.lib.mathematics.units.nativeunits.NativeUnitModel

abstract class AbstractFalconEncoder<T : SIUnit<T>>(
    val model: NativeUnitModel<T>
) : FalconEncoder<T> {
    override val position: Double get() = model.fromNativeUnitPosition(rawPosition)
    override val velocity: Double get() = model.fromNativeUnitVelocity(rawVelocity)
}