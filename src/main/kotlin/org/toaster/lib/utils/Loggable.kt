package org.toaster.lib.utils

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard
import kotlin.reflect.KProperty

// Delegate class to call the 'log' method when the variable is modified
abstract class LoggableDelegate<T>(private var value: T) {
    operator fun getValue(thisRef: Any?, property: KProperty<*>): T = value

    operator fun setValue(thisRef: Any?, property: KProperty<*>, newValue: T) {
        value = newValue
        log(value)
    }

    abstract fun log(newValue: T)
}

// Log to stdout when the variable is initialized and modified
class LogConsole<T>(var value: T, val prefixString: String = "") : LoggableDelegate<T>(value) {
    init {
        log(value)
    }
    override fun log(newValue: T) {
        println(prefixString + newValue)
    }
}

// Log to SmartDashboard when the variable is initialized and modified.
@Suppress("ComplexMethod")
class LogDashboard<T>(var value: T, val dashboardName: String) : LoggableDelegate<T>(value) {
    init {
        log(value)
    }

    override fun log(newValue: T) {
        when(newValue){
            is Double -> SmartDashboard.putNumber(dashboardName, newValue)
            is Number -> SmartDashboard.putNumber(dashboardName, newValue.toDouble())
            is DoubleArray -> SmartDashboard.putNumberArray(dashboardName, newValue)
            is IntArray -> SmartDashboard.putNumberArray(dashboardName, newValue.map { it.toDouble() }.toDoubleArray())
            is Boolean -> SmartDashboard.putBoolean(dashboardName, newValue)
            is BooleanArray -> SmartDashboard.putBooleanArray(dashboardName, newValue)
            is ByteArray -> SmartDashboard.putRaw(dashboardName, newValue)
            is String -> SmartDashboard.putString(dashboardName, newValue)
            else -> System.err.println("Could not log value $newValue to dashboard.")
        }
    }
}

// Log to the dashboard, but don't log the actual value.
class LogDashboardSpecial<T, R>(var value: T, val dashboardName: String, val converter: (T) -> R) : LoggableDelegate<T>(value) {
    init {
        log(value)
    }

    override fun log(newValue: T) {
        val logValue = converter(newValue)
        when(logValue){
            is Double -> SmartDashboard.putNumber(dashboardName, logValue)
            is Number -> SmartDashboard.putNumber(dashboardName, logValue.toDouble())
            is DoubleArray -> SmartDashboard.putNumberArray(dashboardName, logValue)
            is IntArray -> SmartDashboard.putNumberArray(dashboardName, logValue.map { it.toDouble() }.toDoubleArray())
            is Boolean -> SmartDashboard.putBoolean(dashboardName, logValue)
            is BooleanArray -> SmartDashboard.putBooleanArray(dashboardName, logValue)
            is ByteArray -> SmartDashboard.putRaw(dashboardName, logValue)
            is String -> SmartDashboard.putString(dashboardName, logValue)
            else -> System.err.println("Could not log value $logValue to dashboard.")
        }
    }
}

