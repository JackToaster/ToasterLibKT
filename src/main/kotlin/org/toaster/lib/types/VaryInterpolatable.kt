package org.toaster.lib.types

interface VaryInterpolatable<S> : Interpolatable<S> {
    fun distance(other: S): Double
}