package org.toaster.lib.config

import com.ctre.phoenix.motorcontrol.ControlFrame
import com.ctre.phoenix.motorcontrol.StatusFrameEnhanced
import com.ctre.phoenix.motorcontrol.VelocityMeasPeriod
import com.github.salomonbrys.kotson.fromJson
import com.google.gson.JsonSyntaxException
import com.revrobotics.CANSparkMaxLowLevel
import org.toaster.lib.mathematics.units.*
import org.toaster.lib.mathematics.units.nativeunits.DefaultNativeUnitModel
import org.toaster.lib.mathematics.units.nativeunits.SlopeNativeUnitModel
import org.toaster.lib.mathematics.units.nativeunits.nativeUnits
import org.toaster.lib.motors.AbstractFalconMotor
import org.toaster.lib.motors.ctre.FalconSRX
import org.toaster.lib.motors.rev.FalconMAX
import java.io.File

const val kMotorConfigFileName = "motors.json"

// The physical type of motor controller
enum class MotorControllerType {
    SparkMax, TalonSRX, VictorSPX, None
}

// How the motor controller acts when the percent output is 0
enum class NeutralMode {
    Coast, Brake
}

// The type of unit associated with a native unit model
enum class SIUnitType(val unit: SIUnit<*>) {
    Meter(1.meter),
    Degree(1.degree),
    Radian(1.radian),
    Rotation(360.degree)
}

// Configuration for simple & smart current limiting
data class CurrentLimitingConfig(val Continuous: Int, val Peak: Int = 0, val PeakDuration: Int = 0, val Stall: Int = Continuous)

// Configuration for all can frame periods (All in ms)
data class CanFramePeriodConfig(val Control: Int = 5, val MotionControl: Int = 100, val GeneralStatus: Int = 5,
                                val FeedbackStatus: Int = 100, val QuadEncoderStatus: Int = 100,
                                val AnalogTempVbatStatus: Int = 100, val PulseWidthStatus: Int = 100)

// configuration for closed & open loop voltage ramp rates.
data class RampRateConfig(val ClosedLoop: Double = 0.0, val OpenLoop: Double = 0.0)

// Configuration for velocity measurement
data class VelocityMeasurementConfig(val VelocityMeasPeriod: Int = 100, val RollingAverageWindow: Int = 64)

// Configuration for slope native unit models (Converts native units to modelled units)
data class SlopeModelConfig(val ModelledSample: Double = 0.0, val Unit: SIUnitType = SIUnitType.Meter,
                            val NativeUnitSample: Double = 0.0)

// Configuration for a motor controller
data class MotorConfig(val ID: Int,
                       val Follow: String? = null,
                       val Type: MotorControllerType = MotorControllerType.None,
                       val Inverted: Boolean = false,
                       val NeutralMode: NeutralMode? = null,
                       val CurrentLimiting: CurrentLimitingConfig? = null,
                       val CanFramePeriod: CanFramePeriodConfig? = null,
                       val RampRate: RampRateConfig? = null,
                       val VelocityMeasurement: VelocityMeasurementConfig? = null,
                       val SlopeModel: SlopeModelConfig? = null){
    fun getFixedSlopeModel(): SlopeModelConfig?{
        SlopeModel?.let {
            if(it.Unit == null){
                println("Warning: unit not found in slope model for motor with id $ID. Defaulting to Meters.")
                return SlopeModelConfig(it.ModelledSample, SIUnitType.Meter, it.NativeUnitSample)
            }
        }
        return SlopeModel
    }
}

private val motorMap : Map<String, AbstractFalconMotor<*>> by lazy {createMotors()}

fun getMotor(id: String): AbstractFalconMotor<*>? = motorMap[id]

// Create all motor controllers
fun createMotors(): Map<String, AbstractFalconMotor<*>> {
    // Create map of motors
    val motors: MutableMap<String, AbstractFalconMotor<*>> = HashMap()

    // Try to read config; return empty map if failed
    val motorConfigs = readMotorConfig() ?: return motors

    // Create all motor controller objects and put them into the map
    for (entry in motorConfigs) {
        motors[entry.key] = createMotorController(entry.value, motors)
    }

    return motors
}

// Read a file to a string
private fun readFile(name: String): String? = object {}.javaClass.getResource(File.separator + name)?.readText()


// Read the JSON config file to motor config objects
fun readMotorConfig(name: String = kMotorConfigFileName): Map<String, MotorConfig>? {
    val fileStream = readFile(name) ?: return null

    return try {
        GsonProvider.getGson().fromJson(fileStream)
    } catch (e: JsonSyntaxException) {
        println("Could not read JSON file $name due to ${e.message}")
        null
    }
}

// Create an individual motor controller from a motor config object
fun createMotorController(motorConfig: MotorConfig, motors: Map<String, AbstractFalconMotor<*>>)
        : AbstractFalconMotor<*> {

    // Create motor controllers
    val motorController: AbstractFalconMotor<*> = when (motorConfig.Type) {
        MotorControllerType.TalonSRX -> createTalonSRX(motorConfig)
        MotorControllerType.SparkMax -> createSparkMax(motorConfig)
        MotorControllerType.VictorSPX -> createVictorSPX(motorConfig)
        MotorControllerType.None -> TODO("Invalid motor controller type")
    }


    // Configure the properties of the TalonSRX
    with(motorController) {
        // Follow another motor (If it exists)
        motorConfig.Follow?.let {
            motors[it]?.let { other -> follow(other) }
                    ?: println("Could not find motor controller $it to follow.")
        }

        // Set the normal/inverted state of the motor
        outputInverted = motorConfig.Inverted

        // Set the motor to coast/brake mode
        motorConfig.NeutralMode?.let { brakeMode = it == NeutralMode.Brake }
    }

    return motorController
}

@Suppress("ComplexMethod")
fun createTalonSRX(motorConfig: MotorConfig): AbstractFalconMotor<*> {
    val model = motorConfig.getFixedSlopeModel()

    // Create a falconSRX with the native unit model or the default model. This is because Kotlin won't
    // let us create a FalconSRX with a model of type NativeUnitModel<*>, we need a NativeUnitModel<T: SIUnit<T>>
    // and Kotlin has conveniently forgets that all native unit models we create are subtypes of that.
    // There's probably a better way of doing this, but I don't know it yet.
    val falconSRX = model?.let {
        when (it.Unit.unit) {
            is Length ->
                MotorControllerFactory.createFalconSRX(motorConfig.ID, SlopeNativeUnitModel(it.Unit.unit * it.ModelledSample,
                        it.NativeUnitSample.nativeUnits))
            is Rotation2d ->
                MotorControllerFactory.createFalconSRX(motorConfig.ID, SlopeNativeUnitModel(it.Unit.unit * it.ModelledSample,
                        it.NativeUnitSample.nativeUnits))
            else -> MotorControllerFactory.createFalconSRX(motorConfig.ID, DefaultNativeUnitModel)
        }
    } ?: MotorControllerFactory.createFalconSRX(motorConfig.ID, DefaultNativeUnitModel)

    if(falconSRX !is FalconSRX<*>) return falconSRX

    // Configure the motor controller
    with(falconSRX) {
        // Set up peak current, duration, and continuous current.
        motorConfig.CurrentLimiting?.let {
            configCurrentLimit(true,
                    FalconSRX.CurrentLimitConfig(it.Peak.amp, it.PeakDuration.millisecond, it.Continuous.amp))
        }

        motorConfig.CanFramePeriod?.let {
            with(talonSRX) {
                setStatusFramePeriod(StatusFrameEnhanced.Status_1_General, it.GeneralStatus)
                setStatusFramePeriod(StatusFrameEnhanced.Status_2_Feedback0, it.FeedbackStatus)
                setStatusFramePeriod(StatusFrameEnhanced.Status_3_Quadrature, it.QuadEncoderStatus)
                setStatusFramePeriod(StatusFrameEnhanced.Status_4_AinTempVbat, it.AnalogTempVbatStatus)
                setStatusFramePeriod(StatusFrameEnhanced.Status_8_PulseWidth, it.PulseWidthStatus)

                setControlFramePeriod(ControlFrame.Control_3_General, it.Control)
            }
        }

        motorConfig.RampRate?.let {
            with(talonSRX) {
                configOpenloopRamp(it.OpenLoop)
                configClosedloopRamp(it.ClosedLoop)
            }
        }

        motorConfig.VelocityMeasurement?.let {
            with(talonSRX) {
                configVelocityMeasurementPeriod(when (it.VelocityMeasPeriod) {
                    1 -> VelocityMeasPeriod.Period_1Ms
                    2 -> VelocityMeasPeriod.Period_2Ms
                    5 -> VelocityMeasPeriod.Period_5Ms
                    10 -> VelocityMeasPeriod.Period_10Ms
                    20 -> VelocityMeasPeriod.Period_20Ms
                    25 -> VelocityMeasPeriod.Period_25Ms
                    50 -> VelocityMeasPeriod.Period_50Ms
                    100 -> VelocityMeasPeriod.Period_100Ms
                    else -> VelocityMeasPeriod.Period_5Ms
                })
                configVelocityMeasurementWindow(it.RollingAverageWindow)
            }
        }
    }

    return falconSRX
}

fun createVictorSPX(motorConfig: MotorConfig): AbstractFalconMotor<*> {
    val model = motorConfig.getFixedSlopeModel()

    // Create a falconMAX with the native unit model or the default model.
    // Victor SPX does not support setting can frame periods or current limiting.
    return model?.let {
        when (it.Unit.unit) {
            is Length ->
                MotorControllerFactory.createFalconSPX(motorConfig.ID, SlopeNativeUnitModel(it.Unit.unit * it.ModelledSample,
                        it.NativeUnitSample.nativeUnits))
            is Rotation2d ->
                MotorControllerFactory.createFalconSPX(motorConfig.ID, SlopeNativeUnitModel(it.Unit.unit * it.ModelledSample,
                        it.NativeUnitSample.nativeUnits))
            else -> MotorControllerFactory.createFalconSPX(motorConfig.ID, DefaultNativeUnitModel)
        }
    } ?: MotorControllerFactory.createFalconSPX(motorConfig.ID, DefaultNativeUnitModel)
}

@Suppress("ComplexMethod")
fun createSparkMax(motorConfig: MotorConfig): AbstractFalconMotor<*> {
    val model = motorConfig.getFixedSlopeModel()

    // Create a falconMAX with the native unit model or the default model.
    val falconMAX = model?.let {
        when (it.Unit.unit) {
            is Length ->
                MotorControllerFactory.createFalconMAX(motorConfig.ID, SlopeNativeUnitModel(it.Unit.unit * it.ModelledSample,
                        it.NativeUnitSample.nativeUnits))
            is Rotation2d ->
                MotorControllerFactory.createFalconMAX(motorConfig.ID, SlopeNativeUnitModel(it.Unit.unit * it.ModelledSample,
                        it.NativeUnitSample.nativeUnits))
            else -> MotorControllerFactory.createFalconMAX(motorConfig.ID, DefaultNativeUnitModel)
        }
    } ?: MotorControllerFactory.createFalconMAX(motorConfig.ID, DefaultNativeUnitModel)

    if(falconMAX !is FalconMAX<*>) return falconMAX

    // Configure the motor controller
    with(falconMAX) {
        // Set up peak current, duration, and continuous current.
        motorConfig.CurrentLimiting?.let {
            canSparkMax.setSmartCurrentLimit(it.Stall, it.Continuous)
        }

        motorConfig.CanFramePeriod?.let {
            with(canSparkMax) {
                setControlFramePeriodMs(it.Control)
                setPeriodicFramePeriod(CANSparkMaxLowLevel.PeriodicFrame.kStatus0, it.GeneralStatus)
                setPeriodicFramePeriod(CANSparkMaxLowLevel.PeriodicFrame.kStatus1, it.GeneralStatus)
                setPeriodicFramePeriod(CANSparkMaxLowLevel.PeriodicFrame.kStatus2, it.GeneralStatus)
            }
        }

        motorConfig.RampRate?.let {
            with(canSparkMax) {
                openLoopRampRate = it.OpenLoop
                closedLoopRampRate = it.ClosedLoop
            }
        }
    }

    return falconMAX
}
