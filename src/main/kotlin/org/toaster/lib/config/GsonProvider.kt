package org.toaster.lib.config

import com.google.gson.GsonBuilder

object GsonProvider {
    val gsonBuilder = GsonBuilder().setPrettyPrinting()

    fun getGson() = gsonBuilder.create()
}