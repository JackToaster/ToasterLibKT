package org.toaster.lib.config

import org.toaster.lib.mathematics.units.SIUnit
import org.toaster.lib.mathematics.units.nativeunits.NativeUnitModel
import org.toaster.lib.motors.AbstractFalconMotor
import org.toaster.lib.motors.ctre.FalconSPX
import org.toaster.lib.motors.ctre.FalconSRX
import org.toaster.lib.motors.rev.FalconMAX
import org.toaster.lib.simulation.SimFalconMotor

object MotorControllerFactory {
    var isSimulation = false

    fun <T: SIUnit<T>> createOrSimulate(constructor: ()-> AbstractFalconMotor<T>) =
            if(isSimulation){
                SimFalconMotor()
            }else{
                constructor()
            }

    fun <T: SIUnit<T>> createFalconSRX(id: Int, model: NativeUnitModel<T>) =
            createOrSimulate { FalconSRX(id, model) }

    fun <T: SIUnit<T>> createFalconSPX(id: Int, model: NativeUnitModel<T>) =
            createOrSimulate { FalconSPX(id, model) }

    fun <T: SIUnit<T>> createFalconMAX(id: Int, model: NativeUnitModel<T>) =
            createOrSimulate { FalconMAX(id, model) }
}