package org.toaster.lib.wrappers.hid

import edu.wpi.first.wpilibj.GenericHID
import edu.wpi.first.wpilibj.XboxController

typealias FalconXboxController = FalconHID<XboxController>
typealias FalconXboxBuilder = FalconHIDBuilder<XboxController>

// Builder Helpers
fun xboxController(block: FalconXboxBuilder.() -> Unit): FalconXboxController? =
        HIDMap[FalconHIDType.E3D]?.let { XboxController(it).mapControls(block) }

fun xboxController(port: Int, block: FalconXboxBuilder.() -> Unit): FalconXboxController =
    XboxController(port).mapControls(block)

fun FalconXboxBuilder.button(
    button: XboxButton,
    block: FalconHIDButtonBuilder.() -> Unit = {}
) = button(button.value, block)

fun FalconXboxBuilder.triggerAxisButton(
    hand: GenericHID.Hand,
    threshold: Double = HIDButton.DEFAULT_THRESHOLD,
    block: FalconHIDButtonBuilder.() -> Unit = {}
) = axisButton(yTriggerAxisToRawAxis(hand), threshold, block)

// Source Helpers
fun FalconXboxController.getY(hand: GenericHID.Hand) = getRawAxis(yAxisToRawAxis(hand))

fun FalconXboxController.getX(hand: GenericHID.Hand) = getRawAxis(xAxisToRawAxis(hand))
fun FalconXboxController.getRawButton(button: XboxButton) = getRawButton(button.value)

private fun yAxisToRawAxis(hand: GenericHID.Hand) = if (hand == GenericHID.Hand.kLeft) 1 else 5
private fun xAxisToRawAxis(hand: GenericHID.Hand) = if (hand == GenericHID.Hand.kLeft) 0 else 4
private fun yTriggerAxisToRawAxis(hand: GenericHID.Hand) = if (hand == GenericHID.Hand.kLeft) 2 else 3

enum class XboxButton(val value: Int){
    BumperLeft(5),
    BumperRight(6),
    StickLeft(9),
    StickRight(10),
    A(1),
    B(2),
    X(3),
    Y(4),
    Back(7),
    Start(8)
}