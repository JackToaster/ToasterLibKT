package org.toaster.lib.wrappers.hid

import edu.wpi.first.wpilibj.DriverStation
import edu.wpi.first.wpilibj.GenericHID

data class HIDInfo(val typeID: Int = GenericHID.HIDType.kUnknown.value,
                   val buttonCount: Int? = null,
                   val axisCount: Int? = null,
                   val name: String? = null)


// Matches info read from driver station with controller type
// First tries matching typeID from getJoystickType, but skips if the ID is unknown or does not match
// Next tries matching button & axis count
fun HIDInfo.matches(other: HIDInfo) =
        (typeID > 1 && typeID == other.typeID) ||
                (buttonCount?.let{it == other.buttonCount} ?: false &&
                        axisCount?.let{it == other.axisCount} ?: false &&
                        name?.let{other.name?.contains(it) ?: false} ?: false)

fun getHIDInfo(port: Int, driverStation: DriverStation) = with(driverStation){
    HIDInfo(getJoystickType(port), getStickButtonCount(port),
            getStickAxisCount(port), getJoystickName(port))
}

fun createHIDInfoMap(driverStation: DriverStation): Map<Int, HIDInfo> =
        (0 until DriverStation.kJoystickPorts).associateBy({it}, {getHIDInfo(it, driverStation)})

// This is the neatest way to remove null entries from a map, apparently. See below for the "correct" way
// that doesn't create compiler warnings.
@Suppress("UNCHECKED_CAST")
fun associateHIDTypes(infoMap: Map<Int, HIDInfo>): Map<Int, FalconHIDType> =
        infoMap.mapValues { associateHIDType(it.value) }.filter{it.value != null} as Map<Int, FalconHIDType>

/*
fun <K, V> Map<K, V?>.filterNotNull(): Map<K, V> {
    val out = mutableMapOf<K, V>()
    for(entry in this.entries) {
        entry.value.let {
            if (it != null) {
                out[entry.key] = it
            }
        }
    }

    return out
}*/

fun associateHIDType(info: HIDInfo): FalconHIDType? {
    for (type in FalconHIDType.values()) {
        if (type.info.matches(info)) {
            return type
        }
    }
    return null
}

// Reverse the map, and prioritize entries with lower key values.
// This means that if multiple of the same type of controller exist, only the lowest id controller
// will be chosen.
fun <K: Comparable<K>, V> Map<K, V>.reversed(): Map<V, K> =
        entries.sortedByDescending { it.key }.associateBy({ it.value }) { it.key }

fun createHIDMap(driverStation: DriverStation = DriverStation.getInstance()): Map<FalconHIDType?, Int> =
        associateHIDTypes(createHIDInfoMap(driverStation)).reversed()

enum class FalconHIDType(val info: HIDInfo = HIDInfo()){
    PS4(HIDInfo(typeID = 21, axisCount = 6, buttonCount = 14, name = "Wireless Controller")),
    E3D(HIDInfo(typeID = 20, axisCount = 4, buttonCount = 12, name = "Logitech Extreme 3D")),
    Guitar(HIDInfo(typeID = 1, axisCount = 6, buttonCount = 10, name = "Guitar")),
    Dancepad(HIDInfo(typeID = 1, axisCount = 6, buttonCount = 10, name = "Dance Pad"))
}

val HIDMap : Map<FalconHIDType?, Int> by lazy { createHIDMap() }
