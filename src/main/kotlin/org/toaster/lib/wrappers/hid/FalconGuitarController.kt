package org.toaster.lib.wrappers.hid

import edu.wpi.first.wpilibj.Joystick

typealias FalconGuitarController = FalconHID<Joystick>
typealias FalconGuitarBuilder = FalconHIDBuilder<Joystick>

// Builder helpers
fun guitarController(block: FalconGuitarBuilder.() -> Unit): FalconGuitarController? =
        HIDMap[FalconHIDType.E3D]?.let { Joystick(it).mapControls(block) }

fun guitarController(port: Int, block: FalconGuitarBuilder.() -> Unit): FalconGuitarController =
        Joystick(port).mapControls(block)

fun FalconGuitarBuilder.button(
        button: GuitarButton,
        block: FalconHIDButtonBuilder.() -> Unit = {}
) = button(button.value, block)

fun FalconGuitarBuilder.axisButton(
        axis: GuitarAxis,
        threshold: Double = HIDButton.DEFAULT_THRESHOLD,
        block: FalconHIDButtonBuilder.() -> Unit = {}
) = axisButton(axisToRawAxis(axis), threshold, block)


// Source helpers
fun FalconGuitarController.getAxis(axis: GuitarAxis) = getRawAxis(axisToRawAxis(axis))

fun FalconGuitarController.getRawButton(button: GuitarButton) = getRawButton(button.value)

private fun axisToRawAxis(axis: GuitarAxis) = axis.value

enum class GuitarAxis(val value: Int){
    Whammy(4),
    //AccelX(1234), //TODO Check axes for tilt control on guitar
    //AccelY(5678),
    AccelZ(5)
}

enum class GuitarButton(val value: Int) {
    Green(1),
    Red(2),
    Blue(3),
    Yellow(4),
    Orange(5),
    Back(7),
    Start(8)
}