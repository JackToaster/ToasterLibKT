package org.toaster.lib.wrappers.hid

import edu.wpi.first.wpilibj.Joystick

typealias FalconE3DController = FalconHID<Joystick>
typealias FalconE3DBuilder = FalconHIDBuilder<Joystick>

// Builder helpers
fun E3DController(block: FalconE3DBuilder.() -> Unit): FalconE3DController? =
        HIDMap[FalconHIDType.E3D]?.let { Joystick(it).mapControls(block) }

@Suppress("FunctionNaming")
fun E3DController(port: Int, block: FalconE3DBuilder.() -> Unit): FalconE3DController =
        Joystick(port).mapControls(block)

fun FalconE3DBuilder.button(
        button: Int,
        block: FalconHIDButtonBuilder.() -> Unit = {}
) = button(button, block)

fun FalconE3DBuilder.axisButton(
        axis: E3DAxis,
        threshold: Double = HIDButton.DEFAULT_THRESHOLD,
        block: FalconHIDButtonBuilder.() -> Unit = {}
) = axisButton(axisToRawAxis(axis), threshold, block)


// Source helpers
fun FalconE3DController.getAxis(axis: E3DAxis) = getRawAxis(axisToRawAxis(axis))

private fun axisToRawAxis(axis: E3DAxis) = axis.value

enum class E3DAxis(val value: Int){
    Throttle(3),
    Pitch(1),
    Roll(0),
    Yaw(2)
}