package org.toaster.lib.wrappers

import edu.wpi.first.wpilibj.DoubleSolenoid
import edu.wpi.first.wpilibj.Solenoid
import org.toaster.lib.commands.*
import org.toaster.lib.mathematics.units.second
import kotlin.properties.Delegates

/**
 * Interface for both double- and single-solenoids.
 */
interface FalconSolenoid {
    enum class State {
        Forward,
        Reverse,
        Off
    }

    var state: State

    var pulseDuration: Double

    fun pulse()
    fun pulse(duration: Double) {
        pulseDuration = duration
        pulse()
    }
}

/**
 * Single-acting solenoid with only one PCM Solenoid channel.
 * This type of solenoid can only be on or off.
 */
class FalconSingleSolenoid(channel: Int, module: Int? = null) : FalconSolenoid {
    private val wpiSolenoid: Solenoid = if (module == null) Solenoid(channel) else Solenoid(module, channel)

    // Set the solenoid's position. Forward -> true, Reverse -> false
    // If the solenoid is set to 'Off', the state is not changed.
    override var state: FalconSolenoid.State by Delegates.observable(FalconSolenoid.State.Reverse) { _, _, newValue ->
        when (newValue) {
            FalconSolenoid.State.Forward -> wpiSolenoid.set(true)
            FalconSolenoid.State.Reverse -> wpiSolenoid.set(false)
            else -> Unit
        }
    }

    // The type of pulse that will occur when pulse() is called
    enum class PulseType {
        JNI, Command
    }

    var pulseType = PulseType.JNI

    override var pulseDuration: Double by Delegates.observable(0.01) { _, _, newValue ->
        when {
            newValue <= 0.01 -> {
                wpiSolenoid.setPulseDuration(0.01)
                pulseType = PulseType.JNI
            }
            newValue in 0.01..2.55 -> {
                wpiSolenoid.setPulseDuration(newValue)
                pulseType = PulseType.JNI
            }
            else -> pulseType = PulseType.Command
        }
    }

    override fun pulse() = when (pulseType) {
        PulseType.JNI -> wpiSolenoid.startPulse()
        PulseType.Command -> {
            val oldState = state
            state = FalconSolenoid.State.Forward
            sequential {
                +DelayCommand(pulseDuration.second)
                +InstantRunnableCommand { state = oldState }
            }.start()
        }
    }
}

/**
 * Double-acting solenoid with two PCM Solenoid channels.
 * This type of solenoid can be forward (Forward channel is active), reverse (Reverse channel is active),
 * or off (Neither of the two channels are active).
 */
class FalconDoubleSolenoid(forwardChannel: Int, reverseChannel: Int, module: Int? = null) : FalconSolenoid {

    private val wpiSolenoid: DoubleSolenoid =
            if (module == null) {
                DoubleSolenoid(forwardChannel, reverseChannel)
            } else {
                DoubleSolenoid(module, forwardChannel, reverseChannel)
            }

    // Set the solenoid to the desired position
    override var state: FalconSolenoid.State by Delegates.observable(FalconSolenoid.State.Off) { _, _, newValue ->
        when (newValue) {
            FalconSolenoid.State.Forward -> wpiSolenoid.set(DoubleSolenoid.Value.kForward)
            FalconSolenoid.State.Reverse -> wpiSolenoid.set(DoubleSolenoid.Value.kReverse)
            FalconSolenoid.State.Off -> wpiSolenoid.set(DoubleSolenoid.Value.kOff)
        }
    }

    override var pulseDuration: Double = 0.01

    override fun pulse() {
        val oldState = state
        state = FalconSolenoid.State.Forward
        sequential {
            +DelayCommand(pulseDuration.second)
            +InstantRunnableCommand { state = oldState }
        }.start()
    }
}
