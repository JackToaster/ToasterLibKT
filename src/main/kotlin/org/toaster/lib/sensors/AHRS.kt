package org.toaster.lib.sensors

import com.ctre.phoenix.sensors.PigeonIMU
import com.kauailabs.navx.frc.AHRS
import org.toaster.lib.mathematics.units.Rotation2d
import org.toaster.lib.mathematics.units.degree
import org.toaster.lib.utils.Source

fun PigeonIMU.asSource(): Source<Rotation2d> = { fusedHeading.degree }
fun AHRS.asSource(): Source<Rotation2d> = { (-fusedHeading).degree }
