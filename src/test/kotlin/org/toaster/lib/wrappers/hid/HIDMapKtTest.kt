package org.toaster.lib.wrappers.hid


import edu.wpi.first.wpilibj.DriverStation
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito


class HIDMapKtTest {
    val mockDS: DriverStation = Mockito.mock(DriverStation::class.java)

    val infoMap = mapOf(
            0 to HIDInfo(typeID = 21, axisCount = 6, buttonCount = 14, name = "Wireless Controller"),
            1 to HIDInfo(typeID = 20, axisCount = 4, buttonCount = 12, name = "Logitech Extreme 3D"),
            2 to HIDInfo(typeID = 1, axisCount = 6, buttonCount = 10, name = "Guitar (Guitar Hero X-Plorer)"),
            3 to HIDInfo(typeID = 1, axisCount = 6, buttonCount = 10, name = "Dance Pad (Konami Dance Pad)"),
            4 to HIDInfo(0, 5, 6, "bar"),
            5 to HIDInfo(0, 5, 6, "bar")
            )

    val typeMap = mapOf(
            0 to FalconHIDType.PS4,
            1 to FalconHIDType.E3D,
            2 to FalconHIDType.Guitar,
            3 to FalconHIDType.Dancepad
    )

    val hidMap = mapOf(
            FalconHIDType.PS4 to 0,
            FalconHIDType.E3D to 1,
            FalconHIDType.Guitar to 2,
            FalconHIDType.Dancepad to 3
    )

    @Before
    fun setUp() {
        for(entry in infoMap){
            Mockito.`when`(mockDS.getJoystickType(entry.key)).thenReturn(entry.value.typeID)
            Mockito.`when`(mockDS.getStickButtonCount(entry.key)).thenReturn(entry.value.buttonCount)
            Mockito.`when`(mockDS.getStickAxisCount(entry.key)).thenReturn(entry.value.axisCount)
            Mockito.`when`(mockDS.getJoystickName(entry.key)).thenReturn(entry.value.name)
        }
    }

    @Test
    fun matches() {
        // Test match for typeID
        val refID = HIDInfo(typeID = 123, buttonCount = 1, axisCount = 2, name = "bar")

        val matchID = HIDInfo(typeID = 123, buttonCount = 234, axisCount = 345, name = "foo")

        val noMatchID = HIDInfo(typeID = 124, buttonCount = 234, axisCount = 345, name = "foo")

        assert(matchID.matches(refID))
        assert(!noMatchID.matches(refID))

        // Test no match for typeID, but match for button/axis count and name
        val ref = HIDInfo(typeID = 0, buttonCount = 5, axisCount = 6, name = "foobar")

        val match = HIDInfo(typeID = 123, buttonCount = 5, axisCount = 6, name = "bar")

        val noMatch = HIDInfo(typeID = 123, buttonCount = 234, axisCount = 345, name = "foo")

        assert(match.matches(ref))
        assert(!noMatch.matches(ref))
    }

    @Test
    fun getHIDInfo() {
        // Reads one controller info from the driver station
        val info = org.toaster.lib.wrappers.hid.getHIDInfo(0, mockDS)
        
        assert(info.typeID == 21)
        assert(info.buttonCount == 14)
        assert(info.axisCount == 6)
        assert(info.name == "Wireless Controller")
    }

    @Test
    fun createHIDInfoMap() {
        // Ensure that the map generated is the same as the original map
        val generatedMap = org.toaster.lib.wrappers.hid.createHIDInfoMap(mockDS)
        assert(infoMap == generatedMap)
    }

    @Test
    fun associateHIDTypes() {
        val types = org.toaster.lib.wrappers.hid.associateHIDTypes(infoMap)

        assert(types == typeMap)
    }

    @Test
    fun associateHIDType() {
        val ref = FalconHIDType.PS4.info
        val type = org.toaster.lib.wrappers.hid.associateHIDType(ref)

        assert(type == FalconHIDType.PS4)
    }

    @Test
    fun reversed() {
        val inputMap = mapOf(
                1 to "foo",
                2 to "bar",
                3 to "team 3641 is the best",
                4 to "foo"
        )

        val expected = mapOf(
                "foo" to 1,
                "bar" to 2,
                "team 3641 is the best" to 3
        )

        val reversed = inputMap.reversed()

        assert(reversed == expected)
    }

    @Test
    fun createHIDMap() {
        assert(org.toaster.lib.wrappers.hid.createHIDMap(mockDS) == hidMap)
    }
}