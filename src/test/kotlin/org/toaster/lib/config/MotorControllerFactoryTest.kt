package org.toaster.lib.config

import org.junit.Test
import org.toaster.lib.mathematics.units.nativeunits.DefaultNativeUnitModel
import org.toaster.lib.simulation.SimFalconMotor


class MotorControllerFactoryTest {
    @Test
    fun testSimMotorControllerCreation() {
        MotorControllerFactory.isSimulation = true
        val motor = MotorControllerFactory.createFalconMAX(123, DefaultNativeUnitModel)
        val motor1 = MotorControllerFactory.createFalconSPX(123, DefaultNativeUnitModel)
        val motor2 = MotorControllerFactory.createFalconSRX(123, DefaultNativeUnitModel)

        assert(motor is SimFalconMotor)
        assert(motor1 is SimFalconMotor)
        assert(motor2 is SimFalconMotor)
    }
}