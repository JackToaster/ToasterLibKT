package org.toaster.lib.config


import org.junit.Before
import org.junit.Test
import org.toaster.lib.mathematics.units.nativeunits.DefaultNativeUnitModel
import org.toaster.lib.simulation.SimFalconMotor

class MotorConfigKtTest {
    @Before
    fun setUp() {
        MotorControllerFactory.isSimulation = true
    }

    @Test
    fun readMotorConfigSyntaxError() {
        val exception = try{
            readMotorConfig("motorConfig/syntaxError.json")
            false
        }catch(e: Exception){
            println(e)
            true
        }

        assert(!exception) { "There must be no exception when reading an invalid config file." }
    }

    @Test
    fun readMotorConfig(){
        val read = readMotorConfig("motorConfig/AllProperties.json")

        val reference = mapOf("talon" to MotorConfig(ID=10, Follow = null, Type=MotorControllerType.TalonSRX,
                    Inverted=true, NeutralMode=NeutralMode.Brake,
                    CurrentLimiting=CurrentLimitingConfig(Continuous=10, Peak=40, PeakDuration=500, Stall=0),
                    CanFramePeriod=CanFramePeriodConfig(Control=50, MotionControl=50, GeneralStatus=50,
                            FeedbackStatus=50, QuadEncoderStatus=50, AnalogTempVbatStatus=50, PulseWidthStatus=50),
                    RampRate=RampRateConfig(ClosedLoop=10.0, OpenLoop=20.0),
                    VelocityMeasurement=VelocityMeasurementConfig(VelocityMeasPeriod=50, RollingAverageWindow=32)),
                "spark" to MotorConfig(ID=11, Type=MotorControllerType.SparkMax, Inverted=false,
                    NeutralMode=null, Follow = "talon",
                    CurrentLimiting=CurrentLimitingConfig(Continuous=10, Peak=0, PeakDuration=0, Stall=5),
                    CanFramePeriod=null, RampRate=null, VelocityMeasurement=null))

        assert(read == reference)
    }

    @Test
    fun testCreateMotorControllerTalon(){
        val config = MotorConfig(ID=10, Follow = null, Type=MotorControllerType.TalonSRX,
                Inverted=true, NeutralMode=NeutralMode.Brake,
                CurrentLimiting=CurrentLimitingConfig(Continuous=10, Peak=40, PeakDuration=500, Stall=0),
                CanFramePeriod=CanFramePeriodConfig(Control=50, MotionControl=50, GeneralStatus=50,
                        FeedbackStatus=50, QuadEncoderStatus=50, AnalogTempVbatStatus=50, PulseWidthStatus=50),
                RampRate=RampRateConfig(ClosedLoop=10.0, OpenLoop=20.0),
                VelocityMeasurement=VelocityMeasurementConfig(VelocityMeasPeriod=50, RollingAverageWindow=32))

        val motor = createMotorController(config, mutableMapOf())

        assert(motor.outputInverted) {"Motor output should be inverted"}
        assert(motor.brakeMode) {"Motor should be in brake mode"}
    }

    @Test
    fun testCreateMotorControllerSpark(){
        val config = MotorConfig(ID=11, Type=MotorControllerType.SparkMax, Inverted=false,
                NeutralMode=null, Follow = "talon",
                CurrentLimiting=CurrentLimitingConfig(Continuous=10, Peak=0, PeakDuration=0, Stall=5),
                CanFramePeriod=null, RampRate=null, VelocityMeasurement=null)

        val following = MotorControllerFactory.createFalconSRX(123, DefaultNativeUnitModel)
        val motor = createMotorController(config,
                mutableMapOf("talon" to following))

        assert(!motor.outputInverted) {"Motor output should be inverted"}
        assert(!motor.brakeMode) {"Motor should be in brake mode"}
        assert(motor is SimFalconMotor)
        if(motor is SimFalconMotor){
            assert(motor.following == following)
        }
    }

    @Test
    fun testCreateMotorControllerVictor(){
        val config = MotorConfig(ID=11, Type=MotorControllerType.VictorSPX, Inverted=false,
                NeutralMode=null, Follow = null,
                CurrentLimiting=CurrentLimitingConfig(Continuous=10, Peak=0, PeakDuration=0, Stall=5),
                CanFramePeriod=null, RampRate=null, VelocityMeasurement=null)

        val motor = createMotorController(config, mutableMapOf())

        assert(!motor.outputInverted) {"Motor output should be inverted"}
        assert(!motor.brakeMode) {"Motor should be in brake mode"}
    }

    @Test
    fun testCreateMotorControllerBadModel(){
        val read = readMotorConfig("motorConfig/weirdModel.json")
        val motor = createMotorController(read!!["talon"]!!, mutableMapOf())
    }

    @Test
    fun testGetMotorControllerFromMap(){
        // Make sure the motors exist. Reads from motors.json.
        getMotor("talon")!!
        getMotor("spark")!!
    }
}