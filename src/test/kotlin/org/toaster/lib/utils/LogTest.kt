package org.toaster.lib.utils

import org.junit.Test

class LogTest {
    @Test
    fun testLoggingDelegate(){
        var logged = false

        class LogTest<T>(value: T) : LoggableDelegate<T>(value){
            init{
                log(value)
            }
            override fun log(newValue: T) {
                assert(newValue != null)
                logged = true
            }
        }

        var loggedVar: String by LogTest("foo")

        assert(logged)
        assert(loggedVar == "foo")

        logged = false

        loggedVar = "bar"

        assert(logged)
        assert(loggedVar == "bar")
    }
    
    @Suppress("ASSIGNED_BUT_NEVER_ACCESSED_VARIABLE", "UNUSED_VALUE")
    @Test
    fun testDashboardLogTypes() {
        var logDouble by LogDashboard(0.0, "dashboard value")
        logDouble = 1.0

        var logInt by LogDashboard(0, "dashboard value")
        logInt = 1

        var logDoubleArray: DoubleArray by LogDashboard(doubleArrayOf(1.0,2.0,3.0), "dashboard value")
        logDoubleArray = doubleArrayOf(0.0, 1.0, 2.0)

        var logIntArray by LogDashboard(intArrayOf(1, 2, 3), "dashboard value")
        logIntArray = intArrayOf(0, 1, 2)

        var logBoolean by LogDashboard(true, "dashboard value")
        logBoolean = false

        var logBooleanArray by LogDashboard(booleanArrayOf(true, false), "dashboard value")
        logBooleanArray = booleanArrayOf(false, true)

        var logByteArray by LogDashboard(byteArrayOf(1,2,3), "dashboard value")
        logByteArray = byteArrayOf(1,2,3)
    }
}